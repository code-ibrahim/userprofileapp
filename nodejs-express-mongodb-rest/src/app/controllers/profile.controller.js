const db = require("../models");
const Profile = db.profiles;

exports.create = (req, res) => {
    if (!req.body.Image) {
        res.status(400).send({ message: "Content can not be empty!" });
        return;
    }

    const profile = new Profile({
        Image: req.body.Image,
        name: req.body.name,
        id: req.body.id
    });

    profile
        .save(profile)
        .then(data => {
            //res.send(data);
            res.send({
                message: "User addedd successfully"
            });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Profile."
            });
        });
};

exports.findAll = (req, res) => {
    //const Image = req.query.Image;
    //var condition = Image ? { Image: { $regex: new RegExp(Image), $options: "i" } } : {};

    Profile.find({})
        .then(data => {
            if (!data)
                res.status(404).send({ message: "Not found Profiles"});
            else
                res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving profiles."
            });
        });
};

exports.findOne = (req, res) => {
    const id = req.params.id;

    Profile.findById(id)
        .then(data => {
            if (!data)
                res.status(404).send({ message: "Not found profile with id " + id });
            else res.send(data);
        })
        .catch(err => {
            res
                .status(500)
                .send({ message: "Error retrieving Profile with id=" + id });
        });
};

exports.update = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty!"
        });
    }

    const id = req.params.id;

    Profile.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot update Tutorial with id=${id}. Maybe Profile was not found!`
                });
            } else res.send({ message: "Profile was updated successfully." });
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Profile with id=" + id
            });
        });
};

exports.delete = (req, res) => {
    const id = req.params.id;

    Profile.findByIdAndRemove(id)
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot delete Profile with id=${id}. Maybe Tutorial was not found!`
                });
            } else {
                res.send({
                    message: "Profile was deleted successfully!"
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Profile with id=" + id
            });
        });
};


exports.findByName = (req, res) => {
    const name = req.params.name;
    Profile.find({ name: name })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving Profiles."
            });
        });
};

exports.findById = (req, res) => {
    const idVar = req.params.id;
    Profile.find({ id: idVar })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving Profiles."
            });
        });
};