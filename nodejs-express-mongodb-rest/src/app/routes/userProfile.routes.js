module.exports = app => {
    const profiles = require("../controllers/profile.controller.js");

    var router = require("express").Router();

    router.post("/", profiles.create);

    router.get("/", profiles.findAll);

    router.get("/name/:name", profiles.findByName);

    router.get("/id/:id", profiles.findOne);

    router.get("/:id", profiles.findById);

    router.put("/:id", profiles.update);

    router.delete("/:id", profiles.delete);


    app.use('/api/profiles', router);
};