import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { IUser } from './app/Users';

const baseUrl = 'http://localhost:8080/api/profiles/';

@Injectable({
  providedIn: 'root'
})
export class UserRestServiceService {

  constructor(private http: HttpClient) { }


  getUsers(): Observable<IUser[]> {
    console.log("getUsers in user rest service");
    let temVar = this.http.get<IUser[]>('http://localhost:8080/api/profiles/').pipe(catchError(this.errorHandler));;
    console.log("temVar : " + temVar);
    return temVar;
  }

  getUsersByName(name: string): Observable<IUser[]> {
    console.log("getUsersByName in user Rest service");
    let temVar = this.http.get<IUser[]>('http://localhost:8080/api/profiles/name/' + name).pipe(catchError(this.errorHandler));;
    console.log("temVar : " + temVar);
    return temVar;
  }

  getUsersById(id: string): Observable<IUser[]> {
    console.log("getUsers in user Rest service");
    let temVar = this.http.get<IUser[]>('http://localhost:8080/api/profiles/' + id).pipe(catchError(this.errorHandler));;
    //let temVar = this.http.get<IUser>(baseUrl).pipe(catchError(this.errorHandler));;
    console.log("temVar : " + temVar);
    return temVar;
  }

  getUserBy_Id(id: string): Observable<IUser> {
    console.log("getUserBy_Id in user Rest service");
    let temVar = this.http.get<IUser>(`${baseUrl+"/id"}/${id}`).pipe(catchError(this.errorHandler));;
    console.log("temVar : " + temVar);
    return temVar;
  }

  addAdress(data, id: string): Observable<string> {
    console.log("addAdress in user Rest service");
    let temVar = this.http.put<string>(`${baseUrl}/${id}`, data).pipe(catchError(this.errorHandler));
    //let temVar = this.http.put<string>('http://localhost:8080/api/profiles/' + address).pipe(catchError(this.errorHandler));;
    console.log("temVar : " + temVar);
    return temVar;
  }

  deleteUser(id: string): Observable<string> {
    console.log("deleteUser in user Rest service");
    let temVar = this.http.delete<string>(`${baseUrl}/${id}`).pipe(catchError(this.errorHandler));
    console.log("temVar : " + temVar);
    return temVar;
  }

  addUsers(data): Observable<string> {
    console.log("addUsers in user Rest service");
    let temVar = this.http.post<string>(baseUrl, data).pipe(catchError(this.errorHandler));
    console.log("temVar : " + temVar);
    return temVar;
  }

  errorHandler(error: HttpErrorResponse) {
    console.error(error);
    return throwError(error.message || "Server Error");
  } 

}
