import { Component, OnInit } from '@angular/core';
import { UserRestServiceService } from '../../user-rest-service.service';
import { IUser } from '../Users';

@Component({
  selector: 'app-local-profiles-component',
  templateUrl: './local-profiles-component.component.html',
  styleUrls: ['./local-profiles-component.component.css']
})
export class LocalProfilesComponentComponent implements OnInit {

  users: IUser[];
  msg: string;
  images: string[] = [];
  nameSearch: string;

  constructor(private _userRestService: UserRestServiceService) {
    this.getUsers();
  }

  ngOnInit(): void {
  }

  async searchUsersByName() {
    console.log("searchUsersByName clicked");
    this.msg = undefined;
    this.users = undefined;
    this._userRestService.getUsersByName(this.nameSearch).subscribe(
      r => {
        this.users = r;
        this.msg = "success";
        console.log("responce : " + r);
      },
      er => {
        this.msg = "error";
        console.log(er);
      },
      () => console.log("searchUsersByName executed")
    );
    let count = 0;
    while (this.users == undefined && count < 25) {
      count++;
      await this.delay(1000);
    }
    console.log("count : " + count);
    console.log("users : " + this.users);

  }

  private delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }


  async getUsers() {
    console.log("getUsers clicked");
    this.msg = undefined;
    this.users = undefined;
    this._userRestService.getUsers().subscribe(
      r => {
        this.users = r;
        this.msg = "success";
        console.log("responce : " + r);
      },
      er => {
        this.msg = "error";
        console.log(er);
      },
      () => console.log("getUsers executed")
    );
    let count = 0;
    while (this.users == undefined && count < 25) {
      count++;
      await this.delay(1000);
    }
    console.log("count : " + count);
    console.log("users : " + this.users);
    

  }

}
