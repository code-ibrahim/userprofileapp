import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalProfilesComponentComponent } from './local-profiles-component.component';

describe('LocalProfilesComponentComponent', () => {
  let component: LocalProfilesComponentComponent;
  let fixture: ComponentFixture<LocalProfilesComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocalProfilesComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalProfilesComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
