import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponentComponent } from './home-component/home-component.component';
import { LayoutComponentComponent } from './layout-component/layout-component.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { UserServicesService } from '../user-services.service';
import { UserRestServiceService } from '../user-rest-service.service';
import { routing } from './app.routing';
import { LocalProfilesComponentComponent } from './local-profiles-component/local-profiles-component.component';
import { EditComponentComponent } from './edit-component/edit-component.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponentComponent,
    LayoutComponentComponent,
    UserProfileComponent,
    LocalProfilesComponentComponent,
    EditComponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    routing
  ],
  providers: [
    UserServicesService,
    UserRestServiceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
