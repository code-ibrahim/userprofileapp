import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserRestServiceService } from '../../user-rest-service.service';
import { IUser } from '../Users';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  profiles: IUser[];
  profile: IUser;
  id: string;
  msg: string;
  address: string;
  respo: string;
  respoD: string;

  constructor(private _userRestService: UserRestServiceService, private route: ActivatedRoute) {
    this.id = this.route.snapshot.params['id'];
    if (this.id.length > 4) {
      this.getUserById();
    }
    else {
      this.getUsers();
    }    
  }

  ngOnInit(): void {
    //this.id = this.route.snapshot.params['id'];
    //this.getUsers();
  }

  async getUserById() {
    console.log("getUsers clicked. _id:" + this.id);
    this.msg = undefined;
    this.profile = undefined;
    this._userRestService.getUserBy_Id(this.id).subscribe(
      r => {
        this.profile = r;
        this.msg = "success";
        console.log("responce : " + r);
      },
      er => {
        this.msg = "error";
        console.log(er);
      },
      () => console.log("getUsers executed")
    );
    let count = 0;
    while (this.profile == undefined && count < 25) {
      count++;
      await this.delay(1000);
    }
    console.log("count : " + count);
    console.log("user : " + this.profile);

  }

  async getUsers() {
    console.log("getUsers clicked. id:" + this.id);
    this.msg = undefined;
    this.profiles = undefined;
    this._userRestService.getUsersById(this.id).subscribe(
      r => {
        this.profiles = r;
        this.msg = "success";
        console.log("responce : " + r);
      },
      er => {
        this.msg = "error";
        console.log(er);
      },
      () => console.log("getUsers executed")
    );
    let count = 0;
    while (this.profiles == undefined && count < 25) {
      count++;
      await this.delay(1000);
    }
    console.log("count : " + count);
    console.log("users : " + this.profiles);


    if (this.profiles != undefined) {
      for (var i of this.profiles) {
        if (i["Image"] != undefined) {
          console.log(i["Image"]);
          this.profile = i;
          if (i["address"] != undefined) {
            this.address = i["address"];
          }
        }
        else {
          console.log(" iamega is null ");
        }
      }
    }


  }

  async deleteUser() {
    console.log(this.address, this.profile._id);
    
    this.msg = undefined;
    this._userRestService.deleteUser(this.profile._id).subscribe(
      r => {
        this.respoD = r["message"];
        this.msg = "success";
        console.log("responce : " + r["message"]);
      },
      er => {
        this.msg = "error";
        console.log(er);
      },
      () => console.log("addAdress executed")
    );

    let count = 0;
    while (this.respoD == undefined && count < 25) {
      count++;
      await this.delay(1000);
    }
    console.log("count : " + count);
    console.log("user : " + this.respoD);
  }

  addAdress() {
    console.log(this.address, this.profile._id);
    const data = {
      Image: this.profile.Image,
      name: this.profile.name,
      id: this.profile.id,
      address: this.address
    };
    this.msg = undefined;
    this._userRestService.addAdress(data, this.profile._id).subscribe(
      r => {
        this.respo = r["message"];
        this.msg = "success";
        console.log("responce : " + r["message"]);
      },
      er => {
        this.msg = "error";
        console.log(er);
      },
      () => console.log("addAdress executed")
    );

  }


  private delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }


}
