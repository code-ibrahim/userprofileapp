import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { AppComponent } from './app.component';
import { HomeComponentComponent } from './home-component/home-component.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { LocalProfilesComponentComponent } from './local-profiles-component/local-profiles-component.component';
import { EditComponentComponent } from './edit-component/edit-component.component';

const routes: Routes = [
  { path: '', component: HomeComponentComponent },
  { path: 'home', component: HomeComponentComponent },
  { path: 'localProfiles', component: LocalProfilesComponentComponent },
  { path: 'edit', component: EditComponentComponent },
  { path: ':id', component: UserProfileComponent },
  { path: '**', component: HomeComponentComponent }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
