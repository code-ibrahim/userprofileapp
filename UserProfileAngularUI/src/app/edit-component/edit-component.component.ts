import { Component, OnInit } from '@angular/core';
import { UserRestServiceService } from '../../user-rest-service.service';

@Component({
  selector: 'app-edit-component',
  templateUrl: './edit-component.component.html',
  styleUrls: ['./edit-component.component.css']
})
export class EditComponentComponent implements OnInit {

  image: string;
  name: string;
  //id: string;
  respo: string;
  msg: string;


  constructor(private _userRestService: UserRestServiceService) { }

  ngOnInit(): void {
  }

  async addUsers() {
    console.log(this.image + " , " + this.name  );

    const data = {
      Image: this.image,
      name: this.name//,
      //"id": this.id
    };

    this.msg = undefined;
    this._userRestService.addUsers(data).subscribe(
      r => {
        this.respo = r["message"];
        this.msg = "success";
        console.log("responce : " + r["message"]);
      },
      er => {
        this.msg = "error";
        console.log(er);
      },
      () => console.log("addUsers executed")
    );

    let count = 0;
    while (this.respo == undefined && count < 25) {
      count++;
      await this.delay(1000);
    }
    console.log("count : " + count);
    console.log("respo : " + this.respo);
    //if (this.respo == undefined) {
    //  this.respo = "User added Successfully";
    //}
  }


  private delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

}
